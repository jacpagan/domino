#!/usr/bin/env ruby 
=begin
1) Initialize Board
2) Deal Tiles
3) Find First Player
4) Play loop
4a) Play next card
5) Print Dashboard and hands
5a) Print Dashboard
5b) Print Hands
=end

#user is player[0].  If it is his turn, ask for his input

require 'pp'
require 'benchmark'


module Domino 
  
  class Tile 
    attr_accessor :side1, :side2, :tile
    def initialize (side1, side2)
      @side1 = side1
      @side2 = side2
      @tile = "[#{@side1}|#{@side2}]"
    end 
  end 
  
  class Deck  
    attr_accessor :deck, :players, :table
      @@num_dots = 6 #=> number_of_dots per tile side.
    
    def initialize 
      @deck = deal(deck)
      @table = Array.new
    end 
  
    def deal(deck)
      result = Array.new(4) {Array.new}
      for i in 0..deck.size-1
         result[i%4] << deck.delete_at(rand(deck.size))
      end
      result
    end

    def deck
      deck = Array.new
      for i in 0..@@num_dots.to_i
        for j in i..@@num_dots.to_i
          deck << Tile.new(i, j)
         end
        end
      deck
    end

    def print_players
      @deck.each_with_index do |x,index|
        print "\n\tPlayer: #{index+1}\t" 
        x.each do |y|
          print "#{print y.tile} "
        end
      end
      puts "\n\n"
    end

    def first_player_idx
      result=0
      @deck.each_with_index do |x,index|
          x.each do |y|
            if y.side1==@@num_dots and y.side2==@@num_dots
              result=index
            end
          end
      end
      result
    end


    def play_game
      #first move - find player with [6|6] and play 
      @table << first_move(next_player_idx=first_player_idx)
       
      #play until somebody wins or ties the game
      pass=0  #counts how many players have passed in a row, 4 passes = tie game (tranque)
      next_player_idx=(next_player_idx+1)%4 #index (in players)to the next player

      #game loop - loop while players have tiles left
      while @deck.size != 0
        #get input if player 0 or find_next tile if players 1-3
        if next_player_idx == 0 then #user is player[0].  If it is his turn, ask for his input
          tile_index = get_input
        end
        if (next_player_idx == 0 && tile_index ==-1) || next_player_idx > 0 then
          tile_index=find_next_tile(@deck[next_player_idx])  #find next tile (index) to play by next player, or -1  found 
        end
        print "player: #{next_player_idx+1} "
        if tile_index != -1 then
          tile=@deck[next_player_idx].delete_at(tile_index) #delete tile from player and save in tile
          insert_table(tile,match(tile)) #insert tile in table at first matching position
          puts tile.tile
          pass=0       
        else
          puts "pass"
          pass += 1 #player passed, add one to pass to see if game is tied
        end 
        break if @deck[next_player_idx].size == 0 || pass == 4 #end game if player out of tiles or game tied
        next_player_idx = (next_player_idx+1)%4  #get the next player (index in players)
      end

      # print game table and determine who won
      print_table
      print_winner(next_player_idx)
      print_players       
    end

     
   def first_move(first_player_idx)
     tile_index=find_tile(@deck[first_player_idx],@@num_dots,@@num_dots)
     puts "player: #{first_player_idx+1} #{@deck[first_player_idx][tile_index].tile}" 
     @deck[first_player_idx].delete_at(tile_index)
     #get input from player through console
  end
    def get_input
      print_player_options
      num = gets.to_i  
      while num <0 || num > @deck[0].size || (num>0 && match(@deck[0][num-1])==-1) do
        puts "Invalid entry, try again\n"
        print_player_options
        num = gets.to_i
      end
      return num-1  #substract 1 for index number 
    end

    # print available moves (remaining tiles) for the player
    def print_player_options
       print_table
       print "\n\nPick your move: "
       print "pass "
       @deck[0].each_with_index do |x,index|
          print "#{x.tile} "
      end
      puts "?"
      print "return=autoplay   0    "
      @deck[0].each_with_index do |x,index|
          print "#{index+1}     "
      end
      puts
    end

    def print_winner(last_player_idx)
      score=0
      low_score=1000
      winner=-1
      @deck.each_with_index do |x,index|
        player_score=0
        x.each do |y|
          player_score = player_score + y.side1 + y.side2
        end
        score = score + player_score
        if player_score < low_score then 
          low_score = player_score 
          winner = index + 1
        end
      end
      puts "\n\nPlayer #{winner} won!    Score: #{score}\n"
    end

   


    #In player's hand, find index of next tile that can be played (matches either end of table) 
    def find_next_tile(hand)
      i=-1
      hand.each_with_index do |tile,index|
        if match(tile)>0 then #if tile matches either end of table, return its index and break
          i=index
          break
        end
      end      
      i
    end


    #find if tile matches either side of the table and return 1-4 for position of match or -1 for no match
    # (1=left side inverted, 2=right side, 3=left side, 4 = right side inverted, -1=no match)
    def match(tile)
      if tile.side1==@table[0].side1 then 
        n=1  
      elsif tile.side1==@table[-1].side2 then 
        n=2 
     elsif tile.side2==@table[0].side1 then 
        n=3 
      elsif tile.side2==@table[-1].side2 then 
        n=4 
      else n=-1
      end
      return n
    end

    
    #insert tile in table at position position 1-4 (1=left side inverted, 2=right side, 3=left side, 4 = right side inverted)
    def insert_table(tile,pos)
      if pos>0
        @table.insert(if pos==1 || pos==3 then 0 else @table.size end,if pos==1 || pos==4 then invert_tile(tile) else tile end) 
      end
    end




    #turn tile 180 degrees (invert side1 and side2)
    def invert_tile(tile)
      aTile = Tile.new(tile.side2,tile.side1)
      aTile
    end

    #find a tile by its sides within a hand and return its index or -1 if not found 
    def find_tile(hand, side1, side2)
      i=-1
      hand.each_with_index do |x,index|
        if x.side1==side1 && x.side2==side2 then
          i=index
          break
        end
      end
      i
    end   

    #print game table
    def print_table
      print "\nTable:\t" 
      @table.each do |x| 
        print "#{x.tile} "
      end
    end

  end




aDeck = Deck.new
aDeck.print_players
aDeck.play_game


end



iterations = 100_000

Benchmark.bm do |bm|
  # joining an array of strings
  bm.report do
    iterations.times do
      ["The", "current", "time", "is", Time.now].join(" ")
    end
  end

  # using string interpolation
  bm.report do
    iterations.times do
      "The current time is #{Time.now}"
    end
  end

end





    