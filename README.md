Dominos - README

####################################
# to run the code: ./board.rb   inside app/models/ 
####################################

     "a. 1 Player vs 1 CPU --deck=14"
     "b. 1 Player vs 2 CPU --deck=7 "
     "c. 1 Player vs 3 CPU --deck=0 "
     "d. 2 Player vs 2 CPU - team --deck=0 "
     "e. 2 Player vs 2 CPU - indi --deck=0 "
     "f. 2 Player vs 1 CPU --deck=7 "
     "g. 2 Player --deck=14 "
     "g. 3 Player --deck=14 "
     "h. 3 Player vs 1 CPU - team 2 Player && 2player1cpu "



# The game 
# Objective of the game: It should be the objective of every player to strategically play each domino to increase that
player’s chances of winning, or in the event of a partnership, both players chances.
# - generate the set of dominos
# - ask for the number of players that will play

There are 28 dominoes in a pack. 
Each domino has two faces – a blank side and the other filled with dots and is separated into two parts. 
The dots in each part represent a number and the numbers range from 0 to 6.
There are seven (7) of each number (also called “piece”) including the double in a pack.
Therefore, there are seven 1’s, 2’s, 3’s …6’s.

If there are six pieces on the board (that have already been played) and you have the last piece, then you have the hard end.

It should also be decided before the start of the game if
Double Six should pose all games or Winner’s pose (the
winner at the end of each game starts the next game with
a double or suitable piece).

The dominoes MUST be shuffled at the beginning of
each game. 

The highest double, Double Six is then used to start the first game. This is called
the Pose. 

 If a player does not have a matching domino in
hand, then that player will miss that play, (a pass).

Only one domino may be played at each turn except if only that player can play all remaining
dominoes in the game (bow)

Play continues until one player has used the entire dominoes in hand or until no one can play (a
blocked game).
If the game is blocked, the player with the smallest number wins the game. If two players other
than partners count the same amount it is considered a derby no points allocated for that game
but the winner of the next game gets two points.

The first pair to win 6 or 10 games wins the round.
If a pair wins 6 consecutive games then the other pair would have gotten a six love.